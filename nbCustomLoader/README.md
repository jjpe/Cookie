This example project has 2 main elements:

- An action (AwesomeThingy) registered in the java editor toolbar
- A custom setup to break into the loading of java files into the editor

### AwesomeThingy
This action is quite selfexplanatory, just read the javadoc and comments inside.

### DerpMan
The DerpMan classes (DerpObjectMan, DerpSupportMan and DerpLoaderman)
are used to break into the netbeans loading of java files.
The DerpObjectMan is a DataObject and registered with a position lower than the standard JavaDataObject.
Therefore it will be used instead of the standard loading.

The DerpLoaderMan was an attempt at loading the DerpObjectMan and should be unused.
It's there because I was to lazy to check if it can be removed.

The DerpSupportMan extends the DataEditorSupport and is registered into the DerpObjectMan.getCookieSet
for the Cookies it implements.
It mainly overrides the loadFromStreamToKit method to load different data into the StyledDocument of
the java editor than actually is in the DataObject.

It turns out the Java editor uses the Document for its parsing and error checking instead of the DataObject.
This makes this example useless for our purpose, 
but still a good illustration of how the loading of files works in Netbeans.
