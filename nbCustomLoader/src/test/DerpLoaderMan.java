package test;

import java.io.IOException;
import java.util.Set;
import org.netbeans.api.java.loaders.JavaDataSupport;
import org.openide.cookies.EditorCookie;
import org.openide.filesystems.FileObject;
import org.openide.loaders.DataObject;
import org.openide.loaders.DataObjectExistsException;
import org.openide.loaders.MultiDataObject;
import org.openide.loaders.UniFileLoader;

/**
 * This is the loader for DerpObjectMan.
 * It seems to be unused so you can just ignore this.
 * 
 * @author Volker
 */
public class DerpLoaderMan extends UniFileLoader {

    public DerpLoaderMan() {
        super("test.DerpObjectMan");
    }
    
    @Override
    protected MultiDataObject createMultiObject(FileObject primaryFile) throws DataObjectExistsException, IOException {
        throw new UnsupportedOperationException("GOTCHA");
    }
    
}
