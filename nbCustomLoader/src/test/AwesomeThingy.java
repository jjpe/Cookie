package test;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import javax.swing.JEditorPane;
import javax.swing.text.Document;
import org.openide.cookies.EditorCookie;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.awt.ActionRegistration;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.text.Line;
import org.openide.util.Exceptions;
import org.openide.util.NbBundle.Messages;
import org.openide.windows.IOProvider;
import org.openide.windows.InputOutput;

/**
 * This is the stupid green thingy in the editor toolbar.
 * Note that I changed the ActionReference annotation to get it into the editor toolbar.
 * 
 * @author Volker
 */
@ActionID(
        category = "Edit",
        id = "test.AwesomeThingy")
@ActionRegistration(
        iconBase = "test/testicon16x16.png",
        displayName = "#CTL_AwesomeThingy")
@ActionReference(path = "Editors/text/x-java/Toolbars/Default", position = 400)
@Messages("CTL_AwesomeThingy=AWESOMETHINGY")
public final class AwesomeThingy implements ActionListener {

    private final EditorCookie context;

    public AwesomeThingy(EditorCookie context) {
        this.context = context;
    }

    @Override
    public void actionPerformed(ActionEvent ev) {
        // Just some random code for now
        InputOutput io = IOProvider.getDefault().getIO("AWESOME OUTPUT", false);
        System.out.println(io);
        io.getOut().println("Starting awesome stuff:");
        JEditorPane jep = context.getOpenedPanes()[0];
        Document doc = jep.getDocument();
        context.getLineSet().getCurrent(5).show(Line.ShowOpenType.OPEN, Line.ShowVisibilityType.FOCUS);
        try {
            FileObject fo = FileUtil.createMemoryFileSystem().getRoot().createData("Lol", "java");
            
            io.getOut().println("");
        } catch (IOException ex) {
            Exceptions.printStackTrace(ex);
            throw new IllegalStateException("jemoeder!");
        }
    }
}
