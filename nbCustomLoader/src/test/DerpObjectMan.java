/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package test;

import java.io.IOException;
import org.netbeans.api.java.loaders.JavaDataSupport;
import org.netbeans.core.spi.multiview.MultiViewElement;
import org.netbeans.core.spi.multiview.text.MultiViewEditorElement;
import org.openide.cookies.EditorCookie;
import org.openide.filesystems.FileObject;
import org.openide.loaders.DataFolder;
import org.openide.loaders.DataObject;
import org.openide.loaders.DataObjectExistsException;
import org.openide.loaders.MultiDataObject;
import org.openide.loaders.MultiFileLoader;
import org.openide.nodes.Node;
import org.openide.util.Lookup;
import org.openide.windows.IOProvider;
import org.openide.windows.InputOutput;
import org.openide.windows.TopComponent;

/**
 * Registers this DataObject as the factory for obtaining DataObjects from java files.
 * The position=50 is higher priority than the position=100 of the JavaDataObject,
 * allowing us to break into the process.
 * 
 * The idea is to add a custom CloneableEditorSupport or DataEditorSupport.
 * This custom class will override the loadFromStreamToKit and saveFromKitToStream
 * methods to allow us to break into what gets loaded from the file/data object
 * into the editor's StyledDocument.
 * 
 * Hopefully anyway...
 * @author Volker
 */
@DataObject.Registration(
        mimeType="text/x-java",
        position=50)
public class DerpObjectMan extends MultiDataObject {
    
    public DerpObjectMan(FileObject pf, MultiFileLoader loader) throws DataObjectExistsException {
        super(pf, loader);
        InputOutput io = IOProvider.getDefault().getIO("DerpObjectMan sais", false);
        getCookieSet().add(new DerpSupportMan(this));
        io.getOut().println(getCookie(EditorCookie.class));
    }
    
    public @Override Node createNodeDelegate() {
        return JavaDataSupport.createJavaNode(getPrimaryFile());
    }

    @Override
    protected int associateLookup() {
        return 1;
    }
}
