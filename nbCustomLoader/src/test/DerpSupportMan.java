/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package test;

import java.io.IOException;
import java.io.InputStream;
import javax.swing.text.BadLocationException;
import javax.swing.text.EditorKit;
import javax.swing.text.StyledDocument;
import org.openide.cookies.CloseCookie;
import org.openide.cookies.EditCookie;
import org.openide.cookies.EditorCookie;
import org.openide.cookies.LineCookie;
import org.openide.cookies.OpenCookie;
import org.openide.cookies.PrintCookie;
import org.openide.filesystems.FileLock;
import org.openide.filesystems.FileObject;
import org.openide.loaders.DataObject;
import org.openide.loaders.MultiDataObject;
import org.openide.loaders.SaveAsCapable;
import org.openide.text.DataEditorSupport;

/**
 * This is the minimal custom DataEditorSupport.
 * The only interesting method here is the loadfromStreamToKit,
 * which alters what gets read from the file.
 * 
 * @author Volker
 */
public class DerpSupportMan extends DataEditorSupport 
        implements EditorCookie, EditorCookie.Observable, OpenCookie,
        EditCookie, PrintCookie, CloseCookie, SaveAsCapable, LineCookie {
    
     public DerpSupportMan(DataObject obj) {
        super(obj, new Environment(obj));
        setMIMEType("text/x-java");
     }
     
     @Override
     protected void loadFromStreamToKit(StyledDocument doc, InputStream stream, EditorKit kit)
             throws IOException, BadLocationException {
         
       super.loadFromStreamToKit(doc, new InputStream() {
            private int i = 0;
            // D E R P EOF
            private int[] derp = {68, 69, 82, 80, 26};
            @Override
            public int read() throws IOException {
                return i<derp.length ? derp[i++] : -1;
            }
        }, kit);
     }
     
     
     private static class Environment extends DataEditorSupport.Env {
         
        private final DataObject dobj;
        
        public Environment(DataObject dobj) {
            super(dobj);
            this.dobj = dobj;
        }
         
        @Override
        protected FileObject getFile() {
            return dobj.getPrimaryFile();
        }

        @Override
        protected FileLock takeLock() throws IOException {
            return ((MultiDataObject)dobj).getPrimaryEntry().takeLock();
        }
         
    }
}
