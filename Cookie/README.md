## Project Description
This project is an example of how we can wrap user written Java code
in a valid java program and folding out all the generated wrapper code.

The main work of this application is done by the FoldingDataEditorSupport.

Note that the folding is currently not working, due to issue #1.

## Functional Details
### Folding

Folding is done by adding the following xml comments in the java source file (or hopefully Document):
```
//<editor-fold defaultstate="collapsed">
    generated wrapper code to fold
//</editor-fold>
```

The java editor will then automatically collapse it.

Also see issue #1.

### Custom handling of Java files

We use a custom DataObject specification for the `text/x-java` MIME type (`FoldingDataObject`).
This DataObject is registered at a higher priority than the standard JavaDataObject
to get our application between the DataObject (the file on the system) and the StyledDocument (which is displayed in the editor).

#### Loading a Java file
If a Java file is opened in the editor, `FoldingDataEditorSupport.open` is called.
This eventually leads to the DataObject being loaded into StyledDocument
by the `FoldingDataEditorSupport.loadFromStreamToKit` method.

We expect the `.java` file to actually only be the user's content (e.g. an expression or some statements).
In `loadFromStreamToKit` we then wrap the current template around this content
and that will be displayed in the StyledDocument.
Remember to use the `EditorKit.read` methods in favor of directly inserting into the StyledDocument.
The `EditorKit` will take the document's style into account!

NOTE: Of course this does mean that normal java files can no longer be loaded in the netbeans editor,
as we will wrap them into a template too.
See issue #7.

#### Keeping track of user content
When wrapping the user content into the template,
we create a `Position.Bias.Backward` biased Position before the user content and a 
`Position.Bias.Forward` biased Position after it to allow us to easily retrieve the user's content.

The current user content (as displayed in the editor) can be retrieved using `FoldingDataEditorSupport.getUserContent`.
The user content that was last saved to file is recorded in the `FoldingDataEditor.tmp` in-memory file.

### User Interface
The user interface consists of the netbeans java editor with an extra action inserted into the editor's toolbar.

To use the java editor, we use the `JavaDataSupport.createJavaNode` method to obtain a proper java node delegate
for our `FoldingJavaDocument` (or at least I think that is what it does ;p ).

The registration of the toolbar `Action` is done by the `SwitchContextAction`.
The actual dropdown, as well as the handling of user events is done in `SwitchContextDropdown`
(don't ask... it's Netbeans being weird).

## Implementation Details
### Closing editors
When the editor is closed, the template will be stripped of the java file.
To listen to people closing the Netbeans IDE itself, we need an annotated Runnable.
This task is implemented in `MainCloser` and it is responsible for properly closing all remaining open editors.

### Running Java files
The java editor uses the Document (i.e. what you see in the editor) for editor support and java hints.
To run java files however, the actual file on disk is used.
Therefore we load the user's content from the file, wrap it in a template and immediately save it
when we `FoldingDataEditorSupport.open` a file.

As soon as you close the editor, we will extract the user content and write that into the file on disk.
This means that the code can only be run when it is opened in the editor (I believe this is the same in Linqpad).

Eventually we might want to let the user save as a proper java file
(e.g. by selecting the dropdown's `TODO-regular file` option).
See issue #7 for more ideas.

### Changing the Document's content
The changes in the document (wrapping content in templates) are actually applied by using `NbDocument.runAtomic`,
to allow the user to undo them in one go.
The `Runnable` responsible for this is `FoldingDataEditorSupport.TemplateSwitcher`.

NOTE: at the moment you can undo the change, but the toolbar will not properly update, breaking its functionality.
See issue #8.


