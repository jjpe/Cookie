package cookie;

import java.util.HashMap;
import java.util.Map;
import org.netbeans.api.java.classpath.ClassPath;
import org.openide.filesystems.FileObject;
import org.openide.loaders.DataObject;

/**
 *
 * A less ugly way to obtain the wrapper templates for
 * Expressions, Statements and Class context.
 *
 * @author Volker
 * @author Joey
 *
 */
public class TemplateProvider {

    private DataObject dob;
    private final Map<String, Template> m = new HashMap<String, Template>();
    public final static String CLASS = "Class";
    public final static String EXPR = "Expression";
    public final static String STMT = "Statement";
    public final static String NONE = "Regular java";

    public TemplateProvider(DataObject dob) {
        if (!isTemplatable(dob)) {
            throw new UnsupportedOperationException(
                    "Can not provide a template for untemplatable file "
                    + dob.getName());
        }
        this.dob = dob;
        m.put(CLASS, classTemplate());
        m.put(EXPR, exprTemplate());
        m.put(STMT, stmtTemplate());
        m.put(NONE, null);
    }

    public boolean hasTemplateFor(String name) {
        return m.keySet().contains(name);
    }

    public Template get(String type) {
        return m.get(type);
    }

    private Template classTemplate(boolean isParent, String... importClasses) {
        return new Template(CLASS,
                getClassStartTemplate(importClasses),
                isParent ? "}\n" : "\n}\n",
                "public static void main(String[] args) {\n\t",
                "\n}");
    }

    private Template classTemplate(String... importClasses) {
        return classTemplate(false, importClasses);
    }

    private Template stmtTemplate(boolean isParent) {
        boolean hasParent = true;
        return new Template(STMT,
                classTemplate(hasParent).getHeader()
                + "\tpublic static void main(String[] args) {\n",
            (isParent ? "\t}\n" : "\n\t}\n")
                + classTemplate(hasParent).getFooter());
    }

    private Template stmtTemplate() {
        return stmtTemplate(false);
    }

    private Template exprTemplate(boolean isParent) {
        boolean hasParent = true;
        return new Template(EXPR,
                stmtTemplate(hasParent).getHeader()
                + "\t\tSystem.out.println(\n",
                 "\n\t\t);\n"
                + stmtTemplate(hasParent).getFooter());
    }

    private Template exprTemplate() {
        return exprTemplate(false);
    }

    /**
     *
     * @param imports An array containing the fully-qualified names of classes
     * to import, represented as Strings
     * @return
     */
    private String getClassStartTemplate(String... imports) {
        FileDissector fd = new FileDissector(dob);
        StringBuilder builder = new StringBuilder();

        declareClass(builder, fd.className());
        return builder.toString();
    }

    public static String getPackageDecl(DataObject dob) {
        return "package " + new FileDissector(dob).fqn() + ";\n";
    }

    private static void declareClass(StringBuilder sb, String name) {
        sb.append("public class ").append(name).append(" {\n");
    }

    /**
     * Checks whether the file corresponding to the DataObject and its parent
     * folder are writable. If not, then we can not edit it, and so all the
     * templating features should be disabled.
     *
     * @return true iff the file and parent folder are writable.
     */
    public static boolean isTemplatable(DataObject dob) {
        FileObject file = dob.getPrimaryFile();
        return (file.canWrite() && file.getParent().canWrite());
    }

    /**
     * Cuts through FileObjects like a high-energy gamma ray burst cuts through
     * a human being:
     *
     *    * * * M
     *      * * * O
     *        * * - * * O
     *           / + \ B
     *    **** * + + + * <- neutron star collapsing into massive singularity>
     * \ + /
     *        * * - * *
     *      * * . <-- humans on a puny earth> * * *
     *
     */
    private static class FileDissector {

        private FileObject fileObject;

        public FileDissector(DataObject dataObject) {
            this.fileObject = dataObject.getPrimaryFile();
        }

        public String fqn() {
            return ClassPath
                    .getClassPath(fileObject, ClassPath.SOURCE)
                    .getResourceName(fileObject.getParent())
                    .replace("/", ".");
        }

        public String className() {
            return fileObject.getName();
        }
    }

    public class Template {

        private String type;
        private String header;
        private String footer;
        private String innerHeader;
        private String innerFooter;

        private Template(String type, String header, String footer) {
            this(type, header, footer, "", "");
        }

        private Template(String type, String header, String footer, String innerHeader, String innerFooter) {
            this.type = type;
            this.header = header;
            this.footer = footer;
            this.innerHeader = innerHeader;
            this.innerFooter = innerFooter;
        }

        public String getType() {
            return type;
        }

        public String getHeader() {
            return header;
        }

        public String getFooter() {
            return footer;
        }

        // The inner header and footer are content that should be wrapped
        // around the user content and now be considered
        // as part of the user content.
        
        public String getInnerHeader() {
            return innerHeader;
        }

        public String getInnerFooter() {
            return innerFooter;
        }
    }
}
