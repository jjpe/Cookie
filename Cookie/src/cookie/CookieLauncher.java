/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cookie;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.io.PrintWriter;
import org.netbeans.api.java.project.JavaProjectConstants;
import org.netbeans.api.project.Project;
import org.netbeans.api.project.ProjectUtils;
import org.netbeans.api.project.SourceGroup;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.awt.ActionReferences;
import org.openide.awt.ActionRegistration;
import org.openide.cookies.OpenCookie;
import org.openide.filesystems.FileObject;
import org.openide.loaders.DataObject;
import org.openide.util.Exceptions;
import org.openide.util.NbBundle.Messages;

@ActionID(
        category = "File",
        id = "cookie.CookieLauncher")
@ActionRegistration(
        displayName = "#CTL_CookieLauncher",
        iconBase = "cookie/cookie_icon.png")
@ActionReferences({
    @ActionReference(path = "Menu/File", position = 250),
    @ActionReference(path = "Projects/Actions", position = 0),
    @ActionReference(path = "Toolbars/File", position = 0),})
@Messages("CTL_CookieLauncher=New Cookie")
public final class CookieLauncher implements ActionListener {

    private final Project context;

    public CookieLauncher(Project context) {
        this.context = context;
    }

    @Override
    public void actionPerformed(ActionEvent ev) {
        PrintWriter out = null;
        try {
            //find source folder
            FileObject fileObject;
            SourceGroup[] sg = ProjectUtils.getSources(context)
                    .getSourceGroups(JavaProjectConstants.SOURCES_TYPE_JAVA);
            //if there if more than 0, than take the first one.
            if (sg.length > 0) {
                fileObject = sg[0].getRootFolder();

                //create cookies folder in src if not already existed.
                if (!existFolder(fileObject)) {
                    fileObject = fileObject.createFolder("cookies");
                } else {
                    fileObject = fileObject.getFileObject("cookies");
                }

                //check sequence no. of cookies.
                int seqNo = getSeqNo(fileObject);

                //create .cookie file and .java file
                FileObject cookieFile = fileObject.createData("cookie" + seqNo, "cookie");
                out = new PrintWriter(cookieFile.getOutputStream());
                out.println("Expression");
                out.println("0");
                // close the stream already to return file lock, as it is required by the editor
                out.close();
                FileObject javaFile = fileObject.createData("cookie" + seqNo, "java");

                //open the files
                DataObject.find(javaFile).getLookup().lookup(OpenCookie.class).open();
            } else {
                throw new IllegalStateException("The cookie plugin only works for java projects.");
            }
        } catch (IOException ex) {
            Exceptions.printStackTrace(ex);
        } finally {
            if (out != null) {
                out.close();
            }
        }
    }

    public boolean existFolder(FileObject fileObject) {
        boolean folderExists = false;
        FileObject[] subfolders = fileObject.getChildren();
        for (FileObject folder : subfolders) {
            if (folder.getName().equals("cookies")) {
                folderExists = true;
            }
        }
        return folderExists;
    }

    public int getSeqNo(FileObject fileObject) {
        FileObject[] cookies = fileObject.getChildren();
        int seqNo = 1;

        if (cookies != null && cookies.length > 0) {
            for (FileObject cookie : cookies) {
                try {
                    String name = cookie.getName().replace("cookie", "");
                    seqNo = Math.max(seqNo, Integer.parseInt(name));
                } catch (Exception ex) {
                    Exceptions.printStackTrace(ex);
                }
            }
            seqNo++;
        }
        return seqNo;
    }
}
