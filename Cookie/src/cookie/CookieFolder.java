package cookie;

import cookie.FoldingDataEditorSupport.State;
import cookie.FoldingDataEditorSupport.TemplateListener;
import javax.swing.event.DocumentEvent;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import org.netbeans.api.editor.fold.Fold;
import org.netbeans.api.editor.fold.FoldType;
import org.netbeans.spi.editor.fold.FoldHierarchyTransaction;
import org.netbeans.spi.editor.fold.FoldManager;
import org.netbeans.spi.editor.fold.FoldOperation;
import org.openide.loaders.DataObject;
import org.openide.util.Exceptions;

/**
 * Shouldn't cookies crumble rather than fold?
 *
 * @author Joey, Volker
 */
public class CookieFolder implements FoldManager {

    public static final FoldType CLASS = new FoldType(TemplateProvider.CLASS);
    public static final FoldType EXPR = new FoldType(TemplateProvider.EXPR);
    public static final FoldType STMT = new FoldType(TemplateProvider.STMT);

    protected FoldOperation foldOperation = null;
    private FoldingDataEditorSupport support = null;
    private boolean released = false;
    private final TemplateListener listen = new TemplateListener() {
        @Override
        public void onChanged(State state) {
            if (State.DOCUMENT_LOADED.equals(state) && !released) {
                addFold();
            }
        }
    };
    private Fold f1;
    private Fold f2;
    
    /**
     * Helper method which delegates to initFold.
     * Creates the transaction.
     */
    private void addFold() {
        FoldHierarchyTransaction t = foldOperation.openTransaction();
        try {
            removeFolds(t);
            initFolds(t);
        } finally {
            t.commit();
        }
    }

    private void removeFolds(FoldHierarchyTransaction t) {
        foldOperation.removeFromHierarchy(f1, t);
        foldOperation.removeFromHierarchy(f2, t);
        f1=f2=null;
    }
    
    @Override
    public void init(FoldOperation fo) {
        this.released = false;
        this.foldOperation = fo;
        Document doc = foldOperation
                .getHierarchy().getComponent().getDocument();
        Object od = doc.getProperty(Document.StreamDescriptionProperty);

        if (od instanceof DataObject) {
            support = ((DataObject) od).getLookup()
                    .lookup(FoldingDataEditorSupport.class);
            if (support != null) {
                support.listenForTemplate(listen);
            }
        }
    }

    /**
     * Performs a sanity check on a fold offset pair.
     *
     * @return true iff. the fold offset pair is sane.
     */
    protected boolean offsetPairOk(int start, int end) {
        return (start <= end);
    }

    @Override
    public void initFolds(FoldHierarchyTransaction fht) {
        if (support != null) {
            Document doc = support.getDocument();
            // fold 1 spans from start of document to usercontent,
            // but without the newline as that looks bad.
            // hence the -1
            int startOffset1 = 0;
            int endOffset1 = support.getStart().getOffset() - 1;

            // Second fold spans from end of user content
            // including the newline hence the +1
            // to the end of the document, minus the ending newline
            // as that looks bad (hence the -1)
            int startOffset2 = support.getEnd().getOffset() + 1;
            int endOffset2 = doc.getLength() - 1;
            // some redundant checks as the FoldingDataEditorSupport
            // should never be in such a state.
            if (offsetPairOk(startOffset1, endOffset1)
                    && offsetPairOk(startOffset2, endOffset2)) {
                try {
                    f1=foldOperation.addToHierarchy(EXPR, null, true,
                            startOffset1, endOffset1, 0, 0,
                            null, fht);
                    f2=foldOperation.addToHierarchy(EXPR, null, true,
                            startOffset2, endOffset2, 0, 0,
                            null, fht);
                } catch (BadLocationException ex) {
                    Exceptions.printStackTrace(ex);
                }
            }
        }
    }

    // we don't listen to the document, but to the FoldingDataEditorSupport
    
    @Override
    public void insertUpdate(DocumentEvent de, FoldHierarchyTransaction fht) {
    }

    @Override
    public void removeUpdate(DocumentEvent de, FoldHierarchyTransaction fht) {
    }

    @Override
    public void changedUpdate(DocumentEvent de, FoldHierarchyTransaction fht) {
    }

    @Override
    public void removeEmptyNotify(Fold fold) {
    }

    @Override
    public void removeDamagedNotify(Fold fold) {
    }

    @Override
    public void expandNotify(Fold fold) {
    }

    /**
     * Once released we are on vacation forever,
     * so no boss to listen to anymore.
     */
    @Override
    public void release() {
        released = true;
        if (support != null) support.stopListeningForTemplate(listen);
    }
}
