/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cookie;

import java.io.IOException;
import org.netbeans.api.java.loaders.JavaDataSupport;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.filesystems.MIMEResolver;
import org.openide.loaders.DataObjectExistsException;
import org.openide.loaders.FileEntry;
import org.openide.loaders.MultiDataObject;
import org.openide.loaders.MultiFileLoader;

/**
 *
 * @author Volker
 */
@MIMEResolver.ExtensionRegistration(
        displayName = "Cookie file loader",
        mimeType = "text/x-cookie",
        extension = "cookie")
public class CookieFileLoader extends MultiFileLoader {

    public CookieFileLoader() {
        super("cookie.FoldingJavaDataObject");
    }

    @Override
    protected FileObject findPrimaryFile(FileObject fo) {
        if ("java".equals(fo.getExt())) {
            FileObject cookieFile = FileUtil.findBrother(fo, "cookie");
            return cookieFile == null ? null : fo;
        } else if ("cookie".equals(fo.getExt())) {
            return FileUtil.findBrother(fo, "java");
        }
        return null;
    }

    @Override
    protected MultiDataObject createMultiObject(FileObject primaryFile)
            throws DataObjectExistsException, IOException {
        FileObject cookieFile = FileUtil.findBrother(primaryFile, "cookie");
        return new FoldingJavaDataObject(primaryFile, cookieFile, this);
    }

    @Override
    protected MultiDataObject.Entry createPrimaryEntry(
            MultiDataObject obj,
            FileObject primaryFile) {
        return JavaDataSupport.createJavaFileEntry(obj, primaryFile);
    }

    @Override
    protected MultiDataObject.Entry createSecondaryEntry(
            MultiDataObject obj,
            FileObject secondaryFile) {
        return new FileEntry(obj, secondaryFile);
    }

    @Override
    protected String actionsContext () {
        return "Loaders/text/x-java/Actions/"; // NOI18N
    }
}
