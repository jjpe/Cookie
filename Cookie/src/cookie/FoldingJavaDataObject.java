package cookie;

import org.netbeans.api.java.loaders.JavaDataSupport;
import org.openide.filesystems.FileObject;
import org.openide.loaders.DataObjectExistsException;
import org.openide.loaders.MultiDataObject;
import org.openide.loaders.MultiFileLoader;
import org.openide.nodes.CookieSet;
import org.openide.nodes.Node;

/**
 * This DataObject replaces the standard JavaDataObject. Its main action is to
 * register a FoldingDataEditorSupport into the CookieSet.
 *
 *
 * @author Volker
 */
public class FoldingJavaDataObject extends MultiDataObject {

    public FoldingJavaDataObject(FileObject fo,
                                 FileObject sfo,
                                 MultiFileLoader loader)
            throws DataObjectExistsException {
        super(fo, loader);
        if (sfo != null) {
            registerEntry(sfo);
        }
        getCookieSet().add(new FoldingDataEditorSupport(this));
    }

    // I believe this is required to load the java editor
    public @Override
    Node createNodeDelegate() {
        return JavaDataSupport.createJavaNode(getPrimaryFile());
    }
    
    public CookieSet cookies() {
        return getCookieSet();
    }
    
    @Override
    protected int associateLookup() {
        return 1;
    }
}
