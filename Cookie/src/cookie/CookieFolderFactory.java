package cookie;


import org.netbeans.spi.editor.fold.FoldManager;
import org.netbeans.spi.editor.fold.FoldManagerFactory;

/**
 * A factory for CookieFolders.
 * @author Joey
 */
public class CookieFolderFactory implements FoldManagerFactory {

    public CookieFolderFactory() {
    }

    @Override
    public FoldManager createFoldManager() {
        System.out.println(CookieFolderFactory.class.getSimpleName()
                           + ".createFoldManager() called");
        return new CookieFolder();
    }
}
