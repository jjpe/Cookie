package cookie;

import java.awt.Component;
import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.awt.ActionRegistration;
import org.openide.util.NbBundle.Messages;
import org.openide.util.Utilities;
import org.openide.util.actions.Presenter;

/**
 * The action which allows the user to switch the template context. It is a
 * toolbar with SwitchContextDropdown as component.
 *
 * Note that this is just the Netbeans registration of the action.
 * The real work is done in SwitchContextDropdown.
 *
 * @author Volker
 */
@ActionID(
        category = "Build",
        id = "cookie.SwitchContext")
@ActionRegistration(
        lazy = false,
        displayName = "#CTL_SwitchContext")
@ActionReference(path = "Editors/text/x-java/Toolbars/Default", position = 400)
@Messages("CTL_SwitchContext=Switch context")
public final class SwitchContext extends AbstractAction
        implements Presenter.Toolbar {

    // Toolbars apparently require a noargs constructor
    public SwitchContext() {}

    /**
     * Obtains the FoldingDataEditorSupport of the current context.
     *
     * @return the FoldingDataEditorSupport, but note that it may be null.
     */
    private FoldingDataEditorSupport getContext() {
        return Utilities.actionsGlobalContext()
                .lookup(FoldingDataEditorSupport.class);
    }

    @Override
    public void actionPerformed(ActionEvent ev) {
        // handled by the dropdown
    }

    @Override
    public Component getToolbarPresenter() {
        // The context returned at this point,
        // should be the context of the newly opened editor.
        FoldingDataEditorSupport context = getContext();
        // Only display the toolbar for files we can template
        if (context == null
            || !TemplateProvider.isTemplatable(context.getDataObject())) {
            return null;
        } else {
            return new SwitchContextDropdown(context);
        }
    }
}
