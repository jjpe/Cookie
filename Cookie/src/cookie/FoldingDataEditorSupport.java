package cookie;

import cookie.TemplateProvider.Template;
import java.io.BufferedReader;
import java.io.CharArrayReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.Reader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import javax.swing.JEditorPane;
import javax.swing.SwingUtilities;
import javax.swing.text.BadLocationException;
import javax.swing.text.EditorKit;
import javax.swing.text.Position;
import javax.swing.text.StyledDocument;
import javax.swing.undo.AbstractUndoableEdit;
import javax.swing.undo.CannotRedoException;
import javax.swing.undo.CannotUndoException;
import javax.swing.undo.UndoableEdit;
import org.netbeans.editor.BaseDocument;
import org.openide.cookies.CloseCookie;
import org.openide.cookies.EditCookie;
import org.openide.cookies.EditorCookie;
import org.openide.cookies.LineCookie;
import org.openide.cookies.OpenCookie;
import org.openide.cookies.PrintCookie;
import org.openide.cookies.SaveCookie;
import org.openide.filesystems.FileLock;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.loaders.DataObject;
import org.openide.loaders.MultiDataObject;
import org.openide.loaders.SaveAsCapable;
import org.openide.nodes.CookieSet;
import org.openide.text.DataEditorSupport;
import org.openide.text.NbDocument;
import org.openide.util.Exceptions;

/**
 * This DataEditorSupport wraps the content of a file into a template.
 *
 * On document close, it extracts it from the template and save it to the java
 * file.
 *
 * @author Volker
 */
public class FoldingDataEditorSupport extends DataEditorSupport
        implements EditorCookie, EditorCookie.Observable, OpenCookie,
        EditCookie, PrintCookie, CloseCookie, SaveAsCapable,
        LineCookie {

    private static final String EXT = "cookie";
    private Position start;
    private Position end;
    private Position beforeImports;
    private Position afterImports;
    private FileObject brother;
    private String currTemplateType = TemplateProvider.EXPR;
    private TemplateProvider tProvider;
    private final SaveCookie saveImpl = new SaveCookie() {
        /*
         * When saving, we save the java file
         * and record the user's content in the brother file.
         * That will allow the proper java to be written to disk,
         * so the file can be run.
         */
        @Override
        public void save() throws IOException {
            // save the user's content
            if (brother != null) {
                writeBrother();
            }
            // save the valid java document
            saveDocument();
        }

        @Override
        public String toString() {
            return getDataObject().getPrimaryFile().getNameExt();
        }
    };
    private List<TemplateListener> templateObservers =
            new ArrayList<TemplateListener>();

    /**
     * Constructs the FoldingDataEditorSupport.
     *
     * Dev comment: Please note that upon instantiation of this class, the file
     * is not guaranteed to be inside the user's project.
     *
     * @param obj The DataObject which this thing supports.
     */
    public FoldingDataEditorSupport(DataObject obj) {
        super(obj, new Environment(obj));
        setMIMEType("text/x-java");
        brother = FileUtil.findBrother(obj.getPrimaryFile(), EXT);
        initCurrentTemplate();
    }

    private void addSaveCookie() {
        Logger l = Logger.getLogger(FoldingDataEditorSupport.class.getName());
        l.info("Adding savecookie");
        CookieSet s = ((FoldingJavaDataObject) getDataObject()).cookies();
        s.add(saveImpl);
    }

    private void removeSaveCookie() {
        Logger l = Logger.getLogger(FoldingDataEditorSupport.class.getName());
        l.info("Removing savecookie");
        CookieSet s = ((FoldingJavaDataObject) getDataObject()).cookies();
        s.remove(saveImpl);
    }

    @Override
    protected boolean notifyModified() {
        boolean allowed = super.notifyModified();
        if (allowed) {
            addSaveCookie();
        }
        return allowed;
    }

    @Override
    protected void notifyUnmodified() {
        super.notifyUnmodified();
        removeSaveCookie();
    }

    /**
     * Returns the Environment which should be used to obtain streams of the
     * java file on disk.
     *
     * @return the env.
     */
    private DataEditorSupport.Env getEnv() {
        return (DataEditorSupport.Env) env;
    }

    public Position getStart() {
        return start;
    }

    public Position getEnd() {
        return end;
    }

    /**
     * Wraps the actual file's content into java code if the file is
     * templatable.
     *
     * @param doc the document to load the file into.
     * @param stream the stream with the actual file content.
     * @param kit the JavaKit EditorKit.
     * @throws IOException
     * @throws BadLocationException
     */
    @Override
    protected void loadFromStreamToKit(StyledDocument doc,
            InputStream stream,
            EditorKit kit)
            throws IOException, BadLocationException {
        if (!TemplateProvider.isTemplatable(getDataObject())) {
            super.loadFromStreamToKit(doc, stream, kit);
        } else if (TemplateProvider.NONE.equals(getCurrentTemplateType())) {
            super.loadFromStreamToKit(doc, stream, kit);
            start = NbDocument.createPosition(doc, 0, Position.Bias.Backward);
            end = NbDocument.createPosition(
                    doc,
                    doc.getLength(),
                    Position.Bias.Forward);
        } else {
            InputStream in = brother.getInputStream();
            BufferedReader r = new BufferedReader(new InputStreamReader(in));
            NbDocument.runAtomic(doc, new TemplateSwitcher(doc, r, null, true));
            r.close();
        }
    }

    private void initCurrentTemplate() {
        if (brother == null) {
            setCurrentTemplateType(TemplateProvider.NONE);
        } else {
            try {
                setCurrentTemplateType(brother.asLines().get(0));
            } catch (IOException ex) {
                Exceptions.printStackTrace(ex);
            }
        }
    }

    /**
     * Switches the template around the user's code by reloading the document.
     *
     * Note that it should not change the recorded tmp user content, nor should
     * it change the java file on disk
     *
     * @param templateType the type of template to switch to. For now, use the
     * String constants of this class.
     */
    public void switchTemplate(String templateType) {
        if (!getTemplateProvider().hasTemplateFor(templateType)) {
            throw new UnsupportedOperationException(
                    "Template type " + templateType + " is not supported.");
        }
        String oldTemplate = getCurrentTemplateType();
        if (templateType.equals(oldTemplate)) {
            // nothing to switch
            return;
        }
        if (TemplateProvider.NONE.equals(templateType)) {
            try {
                saveImpl.save();
                FileObject fob = getDataObject().getPrimaryFile();
                brother.delete();
                fob.getLookup().lookup(OpenCookie.class).open();
                close(false);
                return;
            } catch (IOException ex) {
                Exceptions.printStackTrace(ex);
                return;
            }
        }
        setCurrentTemplateType(templateType);
        if (brother == null && !TemplateProvider.NONE.equals(templateType)) {
            createBrother();
        }
        // undoing the template switch in the document also requires
        // state changes
        UndoableEdit edit = new MyEdit(this, start, end, oldTemplate);
        Runnable runner = new TemplateSwitcher(
                getDocument(),
                new StringReader(getUserContent()),
                edit);
        NbDocument.runAtomic(getDocument(), runner);
        notifyTemplateListeners(State.DOCUMENT_LOADED);
    }

    /**
     * Creates and sets the brother file which will contain our information.
     *
     * @return the brother file, to allow chaining.
     */
    private FileObject createBrother() {
        FileObject javaFile = getDataObject().getPrimaryFile();
        try {
            this.brother =
                    javaFile.getParent().createData(javaFile.getName(), EXT);
            writeBrother();
        } catch (IOException ex) {
            Exceptions.printStackTrace(ex);
        }
        return this.brother;
    }

    /**
     * Returns the user's current content.
     *
     * Note that the last saved user content is stored in the cookie file.
     * However, this method retrieves the current editor contents!
     *
     * @return the user's content, or null if something went wrong
     *
     * FIXME that null stuff is dirty
     */
    private String getUserContent() {
        try {
            return getDocument().getText(
                    start.getOffset(),
                    end.getOffset() - start.getOffset());
        } catch (BadLocationException e) {
            // FIXME dirty exception handling
            return null;
        }
    }

    private String getImports() {
        try {
            return getDocument().getText(beforeImports.getOffset(),
                    afterImports.getOffset() - beforeImports.getOffset());
        } catch (BadLocationException e) {
            return null;
        }
    }

    /**
     * Write the current contents to the brother file.
     */
    private void writeBrother() {
        PrintWriter out = null;
        FileLock lock = null;
        try {
            lock = brother.lock();
            out = new PrintWriter(brother.getOutputStream(lock));
            out.println(currTemplateType);
            String imports = getImports().trim();
            while (imports.endsWith("\n")) {
                imports = imports.substring(0, imports.length() - 1);
            }
            out.println(imports.length());
            out.println(imports);
            String uc = getUserContent().trim();
            while (uc.startsWith("\n")) {
                uc = uc.substring(1);
            }
            out.print(getUserContent());
        } catch (IOException ex) {
            Exceptions.printStackTrace(ex);
        } finally {
            if (out != null) {
                out.close();
            }
            if (lock != null) {
                lock.releaseLock();
            }
        }
    }

    /**
     * When opening, the correct template should be set.
     */
    @Override
    public void open() {
        super.open();

        // upon open, read the current template from the cookie file (if any)
        initCurrentTemplate();
    }

    // Required class for the constructor of the outer class
    private static class Environment extends DataEditorSupport.Env {

        private final DataObject dobj;

        public Environment(DataObject dobj) {
            super(dobj);
            this.dobj = dobj;
        }

        @Override
        protected FileObject getFile() {
            return dobj.getPrimaryFile();
        }

        @Override
        protected FileLock takeLock() throws IOException {
            return ((MultiDataObject) dobj).getPrimaryEntry().takeLock();
        }
    }

    /**
     * Returns the template provider for this file. It initializes the provider
     * if that has not yet been done.
     *
     * Note that only templatable files can have a provider!
     *
     * @return the template provider.
     */
    private TemplateProvider getTemplateProvider() {
        // only create the template provider for a templatable file
        if (!TemplateProvider.isTemplatable(getDataObject())) {
            throw new IllegalStateException(
                    "Can not get a template provider for a"
                    + " file that can not be edited: "
                    + getDataObject().getName());
        }
        if (tProvider == null) {
            tProvider = new TemplateProvider(getDataObject());
        }
        return tProvider;
    }

    public void listenForTemplate(TemplateListener listener) {
        templateObservers.add(listener);
    }

    public void stopListeningForTemplate(TemplateListener listener) {
        templateObservers.remove(listener);
    }

    private void setCurrentTemplateType(String newTemplateType) {
        if (this.currTemplateType.equals(newTemplateType)) {
            // nothing changed
            return;
        }
        this.currTemplateType = newTemplateType;
        notifyTemplateListeners(State.SWITCHING);
    }

    private void notifyTemplateListeners(State state) {
        for (TemplateListener tl : templateObservers) {
            tl.onChanged(state);
        }
    }

    public String getCurrentTemplateType() {
        return currTemplateType;
    }

    /**
     * This is a runnable intended to be run with NbDocument.runAtomic. It wraps
     * the current template of the given FoldingDataEditorSupport around the
     * given user content. Then it is inserted as the new content of the given
     * document.
     *
     * Note that this runnable also updates the start and end Positions of the
     * FoldingDataEditorSupport
     */
    private class TemplateSwitcher implements Runnable {

        private final StyledDocument doc;
        private final Reader contentReader;
        private final boolean fromCookieFile;
        private final UndoableEdit edit;

        /**
         * Call this constructor when you have the user content as a Stream.
         *
         * @param doc
         * @param support
         * @param userContent
         */
        public TemplateSwitcher(StyledDocument doc,
                InputStream userContent) {
            this(doc, new InputStreamReader(userContent));
        }

        /**
         * Call this constructor when you have the user content as a Reader.
         *
         */
        public TemplateSwitcher(StyledDocument doc,
                Reader userContent) {
            this(doc, userContent, null);
        }

        /**
         * Call this constructor when you have any custom state switching which
         * you would like to bind to this runnable's work. If the user uses the
         * undo or redo buttons or shortcuts, the given edit will be undone or
         * redone together with the changes made to the document.
         */
        public TemplateSwitcher(StyledDocument doc,
                Reader userContent,
                UndoableEdit edit) {
            this(doc, userContent, edit, false);
        }

        /**
         * Call this constructor when you have imports that should be loaded.
         */
        public TemplateSwitcher(StyledDocument doc,
                Reader userContent,
                UndoableEdit edit,
                boolean fromCookieFile) {
            this.doc = doc;
            this.contentReader = userContent;
            this.edit = edit;
            this.fromCookieFile = fromCookieFile;
        }

        /**
         * Clears the document and then reads the start template, user content
         * and end template into the document.
         */
        @Override
        public void run() {
            try {
                EditorKit kit = createEditorKit();

                // Obtain the correct template
                Template template = getTemplateProvider().get(
                        getCurrentTemplateType());
                if (template == null) {
                    // we are dealing with a regular java file
                    // don't change a thing
                    return;
                }
                BufferedReader r = new BufferedReader(contentReader);
                if (fromCookieFile) {
                    // skip template
                    r.readLine();
                    int importLength = Integer.parseInt(r.readLine());
                    doc.remove(0, doc.getLength());
                    kit.read(new StringReader(TemplateProvider.getPackageDecl(
                            getDataObject())), doc, 0);
                    beforeImports = NbDocument.createPosition(doc, doc.getLength(), Position.Bias.Backward);
                    char[] buffer = new char[importLength];
                    r.read(buffer);
                    kit.read(new CharArrayReader(buffer), doc, doc.getLength());
                    r.readLine();
                    doc.insertString(doc.getLength(), "\n", null);
                } else {
                    // Keep imports alive
                    doc.remove(afterImports.getOffset(),
                            doc.getLength() - afterImports.getOffset());
                }
                int afterPos = doc.getLength();
                // load start template
                kit.read(new StringReader(template.getHeader()), doc, doc.getLength());
                afterImports = NbDocument.createPosition(doc, afterPos, Position.Bias.Forward);

                // Register start of user content
                start = NbDocument.createPosition(
                        doc, doc.getLength(), Position.Bias.Backward);
                if (!fromCookieFile) {
                    kit.read(new StringReader(template.getInnerHeader()), doc, doc.getLength());
                }
                kit.read(r, doc, doc.getLength());
                if (!fromCookieFile) {
                    kit.read(new StringReader(template.getInnerFooter()), doc, doc.getLength());
                }
                final int pos = doc.getLength();
                // load end template
                kit.read(new StringReader(template.getFooter()), doc, doc.getLength());

                // Define end position AFTER loading the end template.
                // Otherwise the end template is part of the range due
                // to Bias.Forward
                end = NbDocument.createPosition(
                        doc, pos, Position.Bias.Forward);

                // Set Caret at the right position
                SwingUtilities.invokeLater(new Runnable() {
                    @Override
                    public void run() {
                        JEditorPane[] panes = getOpenedPanes();
                        if (panes != null && panes.length > 0) {
                            JEditorPane pane = panes[0];
                            pane.getCaret().setDot(pos);
                            pane.requestFocus();
                        }
                    }
                });

                /*
                 * If we have been given an edit to group with our current edit,
                 * we should group it.
                 * Note that when a BaseDocument is used, all document changes
                 * performed within the write lock are grouped together.
                 *
                 * If no BaseDocument is being used, we have no way of knowing
                 * what kind of undoable events are triggered and we can't
                 * properly support undoing of template switches.
                 *
                 * If this is the case, the user is just out of luck.
                 * We simply ignore it and hope they don't try to undo a template
                 * switch.
                 *
                 * If they do, it is their own fault for not using a
                 * BaseDocument anyway ;)
                 */
                if (edit != null && doc instanceof BaseDocument) {
                    BaseDocument bdoc = (BaseDocument) doc;
                    bdoc.addUndoableEdit(edit);
                }
                // TODO: figure out how to properly handle the exceptions
            } catch (BadLocationException ex) {
                Exceptions.printStackTrace(ex);
            } catch (IOException e) {
                Exceptions.printStackTrace(e);
            }
        }
    }

    /**
     * An undoable edit which switches the state of the
     * FoldingDataEditorSupport. This should be inside the same group or
     * compoundedit or whatever it is that is being used to undo AND redo
     * multiple edits at once, as the actual switching of the template.
     */
    private class MyEdit extends AbstractUndoableEdit {

        // These fields hold the state to which the FoldingDataEditorSupport
        // should be switched
        private FoldingDataEditorSupport support;
        private Position start;
        private Position end;
        private String templateType;

        public MyEdit(
                FoldingDataEditorSupport support,
                Position oldStart,
                Position oldEnd,
                String oldTemplateType) {
            this.support = support;
            this.start = oldStart;
            this.end = oldEnd;
            this.templateType = oldTemplateType;
        }

        @Override
        public void undo() throws CannotUndoException {
            super.undo();
            handleUndoAndRedo();
        }

        @Override
        public void redo() throws CannotRedoException {
            super.redo();
            handleUndoAndRedo();
        }

        /**
         * Switches the current state of the FoldingDataEditorSupport with the
         * one that was remembered.
         */
        private void handleUndoAndRedo() {
            Position tmp = start;
            start = support.start;
            support.start = tmp;
            tmp = end;
            end = support.end;
            support.end = tmp;
            String tmp2 = templateType;
            templateType = support.getCurrentTemplateType();
            support.setCurrentTemplateType(tmp2);
        }
    }

    public interface TemplateListener {

        public void onChanged(FoldingDataEditorSupport.State state);
    }

    public enum State {
        // switching template, but document not yet loaded

        SWITCHING,
        // switched template and finished updating document
        DOCUMENT_LOADED;
    }
}
