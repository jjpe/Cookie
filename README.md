# Cookie

A Netbeans java-editor plugin to go with your cup o'Java.
Install it as a Netbeans plugin by installing [the plugin file](cookie.nbm).
For more information on how to do that, please consult
[the Standalone Plugin issue](https://github.com/jjpe/Cookie/issues/14).
Or simply go to `tools->plugins->downloaded->add plugins->select cookie.nbm->yes, I agree, etc.`.

## Usage
If the plugin was successfully installed in your Netbeans IDE
you can start creating cookie files.
To do so, you first need to have a Java Project.
This can easily be created via the `file->new->Java Application` wizard.
You are recommended to uncheck the 'create a main class' box, as that would be registered
as the main runnable class.
Then, right click on the *project* in the Projects Overview and select `new Cookie` from the popup menu.
A new cookie file is now created in the source folder of your project, in the `cookies` subfolder.
Alternatively, you can select the *project* and click the big cookie button in the global toolbar.

The main thing that Cookie offers is that it hides the bulk of the java code from view.
This allows you to start writing an expression and simply evaluate it by pressing the standard Run button.
Try it out and type a `5` in your new cookie file. Then save and press run.
If you forgot to uncheck the 'create main class' box, you have to manually point your run configuration to
your new cookie file.

If you are ready to evolve your expression to a statement (or multiple statements)
select the `Statements` template from the dropdown menu in the editor toolbar.
Try it out and change the `5` you typed earlier into
```
int num = 5;
System.out.println(num);
``` 
and press run.

When simple statements no longer suffice for your awesome piece of code,
you can simply evolve to the `Class` template.
This will automatically wrap your statements in a main method and allows you to add methods,
inner classes and everything else that Java allows in a class.
Try it out and you will see the main method now becomes visible around your code.
Add a new method:
```
public static void main(String[] args) {
	System.out.println(getNum());
}
public static int getNum() {
	return 5;
}
```

Finally, there is the `Regular Java` dropdown option.
This will turn your cookie file into a regular java file
and you will no longer have access to the Cookie editor features.
Of course you could always copy-paste the contents back into a new cookie file if you miss the Cookies.

## Project Organization

The project is mainly organized on Github at the [Cookie repository](https://github.com/jjpe/Cookie).
Bugs can be reported as issues on Github and the current development progress should also be tracked
in those issues if discussion is required.

### Directory Structure

#### Cookie
This folder contains the main application.
It is a Netbeans module, and can be turned into a Netbeans plugin using the steps illustrated at
[the Standalone Plugin issue](https://github.com/jjpe/Cookie/issues/14).

#### CopyFQN
This folder contains the code created when following the
[Netbeans CopyFQN tutorial](https://platform.netbeans.org/tutorials/nbm-copyfqn.html).

#### nbCustomLoader
This folder contains example code of how to get between the current Netbeans Java editor
and the actual file on disk.
It provided an entry point for the development of our Netbeans plugin.
